import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Classname: ConsoleApplication
 *
 * <p>Description:
 * Dies ist unsere erste Klasse (Anwendung)
 * </p>
 *
 * @author SNiggoMG
 *
 */
public class ConsoleApplication {

    /**
     * Dies ist die wichtigste Methode
     * 
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String name = "";
        String age = "";
        String hometown = "";

        System.out.println("Hello User");
        System.out.println("Your name please:");
        name = bufferedReader.readLine();
        System.out.println("Your age please:");
        age = bufferedReader.readLine();
        System.out.println("Your hometown please:");
        hometown = bufferedReader.readLine();

        System.out.println("Hello " + name + "(" + age + ") from " + hometown);
    }

}
