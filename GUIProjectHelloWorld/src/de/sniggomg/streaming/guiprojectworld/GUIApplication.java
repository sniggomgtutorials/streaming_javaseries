package de.sniggomg.streaming.guiprojectworld;

import java.awt.Dimension;

import de.sniggomg.streaming.guiprojectworld.mainframe.MainFrame;

public class GUIApplication {

    public static void main(String[] args) {
        MainFrame mainWindow = new MainFrame("Hello World", new Dimension(350, 300));
        mainWindow.setVisible(true);
    }

}
