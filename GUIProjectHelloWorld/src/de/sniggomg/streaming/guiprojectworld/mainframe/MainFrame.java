package de.sniggomg.streaming.guiprojectworld.mainframe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainFrame extends JFrame {

    // Das sind die benötigten Textfelder
    private JTextField nameTF;
    private JTextField ageTF;
    private JTextField hometownTF;
    
    public MainFrame (final String title, final Dimension dimension) {
        this.setTitle(title);
        this.setSize(dimension);
        this.setResizable(false);
        this.setLocationByPlatform(true);
        
        init();
    }

    /**
     * Initialisiert unsere GUI mit allen Labels, InputFields und Buttons
     */
    private void init() {
        JLabel headerLabel  = new JLabel("Hello User");
        this.nameTF         = new JTextField();
        this.ageTF          = new JTextField();
        this.hometownTF     = new JTextField();
        
        this.rootPane.setLayout(new BorderLayout());
        this.rootPane.add(headerLabel, BorderLayout.PAGE_START);
        this.rootPane.add(getUserInputPanel(), BorderLayout.CENTER);
        this.rootPane.add(getButtonPanel(), BorderLayout.PAGE_END);
    }
    
    /**
     * Erzeugt unser ButtonPanel am Ende des Dialogs
     * 
     * @return JPanel mit JButtons
     */
    private JPanel getButtonPanel() {
        JButton readyButton = new JButton("Ready");
        readyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                notifyViaJOptionePane();
            }
        });
        
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        JPanel parentPanel = new JPanel();
        parentPanel.setLayout(new BorderLayout());
        parentPanel.add(readyButton, BorderLayout.WEST);
        parentPanel.add(closeButton, BorderLayout.EAST);
        
        return parentPanel;
    }
    
    /**
     * Erzeugt unser UserInputPanel mit JLabels und JTextFields
     * 
     * @return JPanel mit JLabels und JTextfields
     */
    private JPanel getUserInputPanel() {
        JLabel nameLabel        = new JLabel("Your name please: ");
        JLabel ageLabel         = new JLabel("Your age please: ");
        JLabel hometownLabel    = new JLabel("Your hometown please: ");
        
        JPanel parentPanel = new JPanel();
        parentPanel.setLayout(new GridBagLayout());
        parentPanel.add(nameLabel, new GridBagConstraints( 0, 0
                                                         , 1, 1
                                                         , 1, 1
                                                         , GridBagConstraints.WEST
                                                         , GridBagConstraints.NONE
                                                         , new Insets(1, 1, 1, 1)
                                                         , 0, 0));
        parentPanel.add(nameTF, new GridBagConstraints( 1, 0
                                                      , 1, 1
                                                      , 1, 1
                                                      , GridBagConstraints.WEST
                                                      , GridBagConstraints.HORIZONTAL
                                                      , new Insets(1, 1, 1, 1)
                                                      , 0, 0));
        parentPanel.add(ageLabel, new GridBagConstraints( 0, 1
                                                        , 1, 1
                                                        , 1, 1
                                                        , GridBagConstraints.WEST
                                                        , GridBagConstraints.NONE
                                                        , new Insets(1, 1, 1, 1)
                                                        , 0, 0));
        parentPanel.add(ageTF, new GridBagConstraints( 1, 1
                                                     , 1, 1
                                                     , 1, 1
                                                     , GridBagConstraints.WEST
                                                     , GridBagConstraints.HORIZONTAL
                                                     , new Insets(1, 1, 1, 1)
                                                     , 0, 0));
        parentPanel.add(hometownLabel, new GridBagConstraints( 0, 2
                                                             , 1, 1
                                                             , 1, 1
                                                             , GridBagConstraints.WEST
                                                             , GridBagConstraints.NONE
                                                             , new Insets(1, 1, 1, 1)
                                                             , 0, 0));
        parentPanel.add(hometownTF, new GridBagConstraints( 1, 2
                                                          , 1, 1
                                                          , 1, 1
                                                          , GridBagConstraints.WEST
                                                          , GridBagConstraints.HORIZONTAL
                                                          , new Insets(1, 1, 1, 1)
                                                          , 0, 0));
        return parentPanel;
    }
    
    /**
     * Liest die Inputfields aus und packt sie in einen String und gibt 
     * diesen in einem JOptionPane aus
     */
    private void notifyViaJOptionePane() {
        final String name       = this.nameTF.getText();
        final String age        = this.ageTF.getText();
        final String hometown   = this.hometownTF.getText();
        
        final String resultString = "Hello user " + name + "(" + age + ") from " + hometown;
        
        JOptionPane.showMessageDialog( this
                                     , resultString
                                     , "Hello user"
                                     , JOptionPane.INFORMATION_MESSAGE);
    }
}
